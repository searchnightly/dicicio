import { Bar } from 'vue-chartjs'

export default {
  
  extends: Bar,

  props: ['pDados'],

  //mounted () {

  watch : {

    pDados: function() {
    // Overwriting base render method with actual data.

      var vLabels = new Array;
      var vData = new Array;

      if (this.pDados.length <= 0) {
        return 0
      }

      console.log('pDados tam: ' + this.pDados.length);
      //console.log(this.pDados[0].secao);

      for (var i=0; i < this.pDados.length; i++) {
           vLabels.push(this.pDados[i].secao);
           vData.push(parseFloat(this.pDados[i].totalSalBruto,2));
           console.log(vLabels[i] +' - ' + vData[i]);
      }

      this.renderChart({

        labels: vLabels,
        //labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],

        datasets: [
          {
            label: 'Seções',
            backgroundColor: '#f87979',
            data: vData
            //data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11]
          }
        ]
      })

    }
  }
}