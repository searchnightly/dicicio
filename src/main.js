import Vue from 'vue'
import Vuetify from 'vuetify'
//import VueRouter from 'vue-router'

import App from './App.vue'
 
// Vue.use(Vuetify, {
//   theme: {
//     primary: '#b71c1c',
//     secondary: '#b0bec5',
//     accent: '#8c9eff',
//     error: '#b71c1c'
//   }
// });

Vue.use(Vuetify)

// Vue.use(VueRouter);

// const Attractions = { template: '<div>Attractions</div>' }
// const Breakfast = { template: '<div>Breakfast</div>' }
// const Meat = { template: '<div>Meat</div>' }
// const Sushi = { template: '<div>Sushi</div>' }

// const routes = [
//   { path: '/', component: Attractions },
//   { path: '/breakfast', component: Breakfast },
//   { path: '/meat', component: Meat },
//   { path: '/sushi', component: Sushi },
// ]

// const router = new VueRouter({
//   routes // short for `routes: routes`
// })


new Vue({
  el: '#app',
//  router,
  render: h => h(App)
})
